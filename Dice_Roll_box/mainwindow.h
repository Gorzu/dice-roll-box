#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>	
#include <QSerialPortInfo>
#include <QDebug>
#include <QMessageBox>
#include <QList>
#include <QApplication>
#include <QFile>
#include <QSerialPort>
#include <windows.h>
#include <unistd.h>
#include "geometryengine.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QMouseEvent>
#include <cmath>
#include "MPU.h"


class MainWidget;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/*! \class MainWindow
    \brief Okno główne programu - graficzny interfejs użytkownika
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    MainWidget *mainWidget;

private slots:

    /*!
     * \brief Metoda wyszukująca dostępne urządzenia podłączone do komputera
     */
    void on_actionFind_Board_triggered();

    /*!
     * \brief  Metoda włączająca komunikację z urządzeniem
     */
    void on_actionConnect_triggered();

    /*!
     * \brief Metoda restartująca komunikację z urządzeniem
     */
    void on_actionRESTART_triggered();

    /*!
     * \brief Metoda wyłączająca komunikację z urządzeniem
     */
    void on_actionDisconnect_triggered();

    /*!
     *  \brief Metoda odczytująca w pętli dane z urządzenia
     */
    void readFromArduino();

    /*!
     *  \brief Metoda do ustawienia kostki w pozycji początkowej po wciśnięciu przycisku RESTART
     */
    void on_pushButtonRestartD6_clicked();

public slots:
    /*!
     *  \brief Slot ustawiający graficzny wygląd wykresów
     */
    void makePlot();

    /*!
     *  \brief Slot łączący odpowiednie sloty z sygnałami
     */
    void Connections();

    /*!
     *  \brief Slot rysujący aktualnie odbierane dane na wykresach
     */
    void setChartsValue(const QVector3D &Axis,char ID);


private:
    Ui::MainWindow *ui;
    QSerialPort *device;

    /*!
     * \brief Metoda wysyłająca dane z komputera do urządzenia
     * \param[in] msg - pole przechowujące wiadomość do urządzenia w formie ciągu znaków
     */
    void sendMessageToArduino(QString msg);

    Device *dev; //!< wskaźnik na urządzenie
    QVector<double> Xgyro_y, Ygyro_y, Zgyro_y, dtTime; //!< wektory z danymi do wyświetlenia na wykresach
    QVector<double> Xacc_y, Yacc_y, Zacc_y; //!< wektory z danymi do wyświetlenia na wykresach
    double Time; //!< zmienna czasu
    float dt; //!< zmienna wykorzystywana do aktualizacji osi X na wykresach
};



/*! \class MainWidget
  \brief Klasa wizualizująca sześcienną kostkę do gry
*/
class MainWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    using QOpenGLWidget::QOpenGLWidget;
    //MainWidget(QWidget* parent = nullptr):QOpenGLWidget(parent){};
    ~MainWidget();

    /*!
     *  \brief Metoda obsługująca sterowanie kostką za pomocą zewnętrznego urządzenia
     *  \param[in] accel - wektor danych odebranych z akcelerometru
     */
    void boxShake(QVector3D accel);

protected:
    /*!
     *  \brief Metoda obsługująca wciśnięcie przycisku myszy na widgecie kostki
     *  \param[in] *e - informacja o wciśnięciu przycisku myszy
     */
    void mousePressEvent(QMouseEvent *e) override;

    /*!
     *  \brief Metoda obsługująca zwolnienie przycisku myszy na widgecie kostki
     *  \param[in] *e - informacja o wciśnięciu przycisku myszy
     */
    void mouseReleaseEvent(QMouseEvent *e) override;

    /*!
     *  \brief Metoda obsługująca timer odpowiedzialny za czas, w którym przycisk myszy był wciśnięty
     *  \param[in] *e - informacja o wciśnięciu przycisku myszy
     */
    void timerEvent(QTimerEvent *e) override;


    /*!
     *  \brief Metoda do inicjalizacji widgetu kostki
     */
    void initializeGL() override;

    /*!
     *  \brief Metoda do ustawiania wymiarów widgetu kostki
     *  \param[in] w - szerokość (width) widgetu
     *  \param[in] h - wysokość (height) widgetu
     */
    void resizeGL(int w, int h) override;

    /*!
     *  \brief Metoda rysująca widget z kostką
     */
    void paintGL() override;


    /*!
     *  \brief Metoda do inicjalizacji shaderów
     */
    void initShaders();

    /*!
     *  \brief Metoda do inicjalizacji tekstur kostki
     */
    void initTextures();

private:
    QBasicTimer timer;
    QOpenGLShaderProgram program;
    GeometryEngine *geometries = nullptr;

    QOpenGLTexture *texture = nullptr;

    QMatrix4x4 projection;

    QVector2D mousePressPosition; //!< pozycja kursora w momencie wciśnięcia przycisku myszy
    QVector3D rotationAxis; //!< oś rotacji
    qreal angularSpeed = 0; //!< Prędkość kątowa wykorzystywana do rotacji kostki
    qreal linearSpeed=0; //!< Prędkość liniowa wykorzystywana do translacji kostki
    QQuaternion rotation; //!< Rotacja kostki

    QVector3D accLast; //!< Przedostatnie dane odebrane z akcelerometru

    double translationX = 0.0; //!< Translacja kostki w osi OX
    double translationY = 0.0; //!< Translacja kostki w osi Oy

    QVector3D m;  //!< wektor pomocniczy wykorzystywany do translacji
};


#endif // MAINWINDOW_H
