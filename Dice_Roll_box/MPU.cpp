#include "MPU.h"



Device::Device(QObject *parent) : QObject(parent)
{
    dataStore = new DataStore(0,0,0,0);
}

Device::~Device(){}


void Device::getData(QSerialPort *arduinoPort)
{

    int character;
    std::string separator;


    if(data.find('\n')==std::string::npos)
    {
        data+=arduinoPort->readAll().toStdString();
        qDebug() << data.c_str();
    }
    else
    {
       if(data.at(0)=='G')
       {
           char id = 'G';

           /* X */
           data.erase(0,1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int x = std::atoi(separator.c_str());

           /* Y */
           data.erase(0,character+1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int y = std::atoi(separator.c_str());

           /* Z */
           data.erase(0,character+1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int z = std::atoi(separator.c_str());

           dataStore->setID(id);
           dataStore->setX(x);
           dataStore->setY(y);
           dataStore->setZ(z);

           emit gyroToCharts(QVector3D(dataStore->getX(),dataStore->getY(),dataStore->getZ()),dataStore->getID());
           qDebug() << dataStore->getID() << dataStore->getX() << dataStore->getY( )<< dataStore->getZ();
       }


       if(data.at(0)=='A')
       {

           char id = 'A';

           data.erase(0,1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int x = std::atoi(separator.c_str());

           data.erase(0,character+1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int y = std::atoi(separator.c_str());

           data.erase(0,character+1);
           character = data.find(' ');
           separator = data.substr(0,character);
           int z = std::atoi(separator.c_str());


           dataStore->setID(id);
           dataStore->setX(x);
           dataStore->setY(y);
           dataStore->setZ(z);
           qDebug() << dataStore->getID() << dataStore->getX() << dataStore->getY( )<< dataStore->getZ();
           emit accToCharts(QVector3D(dataStore->getX(),dataStore->getY(),dataStore->getZ()),dataStore->getID());
       }

        emit newDeviceValues();
        data.clear();
    }
}

