var searchData=
[
  ['m_38',['m',['../class_main_widget.html#a0fa80933f0139b4ce4b7e64b72e4fe7b',1,'MainWidget']]],
  ['main_39',['main',['../main_8cpp.html#a0ddf1224851353fc92bfbff6f499fa97',1,'main.cpp']]],
  ['main_2ecpp_40',['main.cpp',['../main_8cpp.html',1,'']]],
  ['mainwidget_41',['MainWidget',['../class_main_widget.html',1,'']]],
  ['mainwidget_42',['mainWidget',['../class_main_window.html#acf2f9067838cc33a677aa8339ad0ac27',1,'MainWindow']]],
  ['mainwidget_2ecpp_43',['mainwidget.cpp',['../mainwidget_8cpp.html',1,'']]],
  ['mainwindow_44',['MainWindow',['../class_main_window.html',1,'MainWindow'],['../class_main_window.html#a996c5a2b6f77944776856f08ec30858d',1,'MainWindow::MainWindow()']]],
  ['mainwindow_2ecpp_45',['mainwindow.cpp',['../mainwindow_8cpp.html',1,'']]],
  ['mainwindow_2eh_46',['mainwindow.h',['../mainwindow_8h.html',1,'']]],
  ['makeplot_47',['makePlot',['../class_main_window.html#a699115cac96d47fd0f8a2bf359c3203c',1,'MainWindow']]],
  ['mousepressevent_48',['mousePressEvent',['../class_main_widget.html#af95f85af6ec3ce4a2355d82d891ed2c0',1,'MainWidget']]],
  ['mousepressposition_49',['mousePressPosition',['../class_main_widget.html#a276db30ce35679020d305f3bdb3384ee',1,'MainWidget']]],
  ['mousereleaseevent_50',['mouseReleaseEvent',['../class_main_widget.html#a4160044e059b7c4f41d710cda7009e4b',1,'MainWidget']]],
  ['mpu_2ecpp_51',['MPU.cpp',['../_m_p_u_8cpp.html',1,'']]],
  ['mpu_2eh_52',['MPU.h',['../_m_p_u_8h.html',1,'']]]
];
